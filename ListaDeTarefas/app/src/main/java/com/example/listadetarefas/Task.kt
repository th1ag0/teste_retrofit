package com.example.listadetarefas

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "tarefas")
 data class Task(

    var title:String = "",
    var description:String = "",
    var status:Boolean = false
) {
    @PrimaryKey(autoGenerate = true)
    var id: Int? = null
    val taskString get() = "[${status}] Titulo: ${title}, Descrição: ${description}"
    override fun toString()= taskString
}