package com.example.listadetarefas.logic

import androidx.appcompat.widget.DialogTitle
import androidx.lifecycle.ViewModel
import com.example.listadetarefas.Task

class TaskViewModel(): ViewModel() {
    val task:Task
    init {
        task = Task()
    }

    fun currentTask(title: String){
        task.title = title
    }
    fun updateDescriptionTask(description: String){
        task.description = description
    }

}