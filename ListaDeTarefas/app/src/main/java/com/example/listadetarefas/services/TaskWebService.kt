package com.example.listadetarefas.services

import com.example.listadetarefas.Task
import retrofit2.Call
import retrofit2.http.GET

interface TaskWebService {
    @GET("tasks")
    fun getAll(): Call<List<Task>>
}