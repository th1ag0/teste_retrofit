package com.example.listadetarefas.ui

import android.content.Intent
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.core.view.isVisible
import androidx.recyclerview.widget.RecyclerView
import com.example.listadetarefas.R
import com.example.listadetarefas.Task
import kotlinx.android.synthetic.main.card_task_large.view.*
import kotlinx.android.synthetic.main.card_task_small.view.*

class TaskAdapter(private var tasks:MutableList<Task>, private var listener: TaskAdapterListener):
    RecyclerView.Adapter<TaskAdapter.ViewHolder>() {

    private var taskEditing: Task? = null

    override fun getItemCount() = tasks.size

    fun saveTask(task: Task, position: Int){
        tasks.set(position, task)
        notifyItemChanged(position)
    }

    fun addCard(task: Task):Int {
        if (tasks.size > 0 && tasks[0].id == null){
            tasks.set(0,task)
            notifyItemChanged(0)
        }else{
            tasks.add(0, task)
            notifyItemInserted(0)
        }
        taskEditing = task
        return 0
    }

    fun deleteTask(position: Int) {
        tasks.removeAt(position)
        notifyItemRemoved(position)
    }

    fun updateTask(task: Task, position: Int) {
        tasks.set(position,task)
        taskEditing = null
        notifyItemChanged(position)
    }

    override fun getItemViewType(position: Int): Int {
        val task = tasks[position]
        return if (taskEditing == task) R.layout.card_task_large else R.layout.card_task_small
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int)= ViewHolder(LayoutInflater
        .from(parent.context)
        .inflate(viewType, parent, false))

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val task = tasks[position]
        holder.fillUI(task)
    }

    inner class ViewHolder(itemView: View):RecyclerView.ViewHolder(itemView){
        fun fillUI(task: Task) {

            if(taskEditing == task) {
                itemView.txtTitleInput.setText(task.title)
                itemView.txtDescriptionInput.setText(task.description)
                itemView.btSave.setOnClickListener {
                    with(this@TaskAdapter) {

                        val title = itemView.txtTitleInput.text.toString()
                        val description = itemView.txtDescriptionInput.text.toString()
                        var status = task.status

                        val position = tasks.indexOf(task)
                        var newTask = Task(title, description, status!!)

                        if (title.isEmpty()) {
                            itemView.txtTitleInput.setError("Campo obrigatório")
                        }
                        if (description.isEmpty()){
                            itemView.txtDescriptionInput.setError("Campo obrigatório")
                        }else{
                            if (task.id == null) {
                                listener.saveTask(newTask,position)
                            }else {
                                newTask.id = task.id
                                updateTask(newTask,position)
                                listener.updateTask(newTask)
                            }
                        }
                    }
                }
            }else {
                if (task.id == null) {
                    itemView.txtTitleView.setText(itemView.context.resources.getString(R.string.task_not_save))
                }else{
                    itemView.txtTitleView.setText(task.title)
                    if (!task.status) {
                        itemView.btShare.isVisible = false
                        itemView.txtStatus.setText(itemView.context.resources.getString(R.string.status_task_not_done))
                        itemView.txtStatus.setTextColor(ContextCompat.getColor(itemView.context,R.color.colorRed))
                    }else{
                        itemView.btShare.isVisible = true
                        itemView.txtStatus.setText(itemView.context.resources.getString(R.string.status_task_done))
                        itemView.txtStatus.setTextColor(ContextCompat.getColor(itemView.context,R.color.colorPrimary))
                    }
                }
                itemView.btDelete.setOnClickListener {
                    val position = tasks.indexOf(task)
                    deleteTask(position)
                    listener.deleteTask(task)
                }
                itemView.btShare.setOnClickListener {
                    val msg = itemView.context.resources.getString(R.string.share_task_message)
                    val sendIntent: Intent = Intent().apply {
                        action = Intent.ACTION_SEND
                        putExtra(Intent.EXTRA_TEXT, "${msg}: ${task.title}")
                        type = "text/plain"
                    }
                    itemView.context.startActivity(sendIntent)
                }
                itemView.setOnClickListener {
                    val position = tasks.indexOf(task)
                    taskEditing = tasks[position]
                    notifyItemChanged(position)
                }
                itemView.setOnLongClickListener {
                    val positon = tasks.indexOf(task)
                    val newTask = tasks[positon]
                    if (newTask.status){
                        newTask.status = false
                    }else{
                        newTask.status = true
                    }
                    updateTask(newTask,positon)
                    listener.updateTask(task)
                    false
                }
            }
        }
    }
}