package com.example.listadetarefas.services

import android.util.Log
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.LiveData
import com.example.listadetarefas.Task
import com.google.gson.Gson
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

class TaskRepository {
    private var webservice: TaskWebService? = null

    fun getAllTasks(): LiveData<Task> {
        val data = MutableLiveData<Task>()
        val retrofit = Retrofit.Builder()
            .baseUrl("http://10.0.2.2/WebService-Slim/rest.php/")
            .addConverterFactory(GsonConverterFactory.create())
            .build()

        webservice = retrofit.create(TaskWebService::class.java)

        webservice!!.getAll().enqueue(object : Callback<List<Task>?>{
            override fun onFailure(call: Call<List<Task>?>?, t: Throwable?) {
                Log.e("onFailure error", t?.message)
            }

            override fun onResponse(call: Call<List<Task>?>?, response: Response<List<Task>?>?) {

                response?.body()?.let {
                    val dado = it

                    for (i in dado) {
                        Log.d("teste", "${i.taskString}")
                    }
                }
            }
        })
        return data
    }


}